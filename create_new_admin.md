## Creating a new admin
1. First, the soon-to-be admin candidate should register in Visas Iespejas (or their country's equivalent)  
2. Then in [`snimdalla`](https://snimdalla.com/) as an admin go to `User administration` - [`All users`](https://snimdalla.com/admin/users)
3. Find the admin candidate via any of the searchable filters like `email`
4. Beneath the `Actions` column find the edit icon and click it 
5. Change the `User role` to `Admin`

![Editing a user](create_admin.png)
