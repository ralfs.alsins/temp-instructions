## Creating a new poster page
1. Enter [snimdalla](https://snimdalla.com/) as an admin
2. Navigate via the side menu to [Youth Opportunities ➡️ Posters](https://snimdalla.com/events/posters)
3. `Create new event poster`
4. Enter the slug
5. Pick any/relevant `Type`.  
Note - if you wish to use `multi-tabs` for poster, all of their `Types` should match.
6. Enter `Title` and `Sub title locale`
7. Upload a `Title image`
8. By _saving/publishing_ you have now created a unique poster page which acts as a single _tab_ with its own structure ([example poster here](https://visasiespejas.lv/poster/pievieno-savu-darba-vietu-go-remote-platformai-bez-maksas)). You can have multiple _tabs_ with independent structures (remember to group them by `Type`). Here is an example of two:

![Two tab example](chrome_Zr48SnZVV9.gif)

9. The actual content is made of `Poster Blocks` found at the bottom of `Poster builder`

Experiment with different components and reorder them to your liking!
